<?php
namespace Sumpay;
class NewCryptAES
{
    protected $secret_key = '';
    
    protected $iv = '';
    
    public function set_key($key)
    {
        $this->secret_key = $key;
    }
    
    public static function getAesKey()
    {
        $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        $length = strlen($chars);
        $key = "";
        for ($i = 0; $i < 32; $i ++) {
            $key .= $chars[rand(0, $length - 1)];
        }
        return $key;
    }
    
    // php 7.1之后的版本
    function encrypt($message)
    {
        $encrypted = openssl_encrypt($message, 'AES-256-ECB', $this->secret_key, OPENSSL_RAW_DATA | PKCS7_NOATTR);
        $encrypt_msg = base64_encode($encrypted);
        return $encrypt_msg;
    }
}
?>  